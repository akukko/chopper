from drawing import *

transition_duration = 0.25

def x(t):
    return max(0.001, (t)/transition_duration)

def y(t, dur):
    return max(0.001, (t - dur + transition_duration)/transition_duration)

def bg_circle(clip, t, opacity = 0.4):
    dur = clip.duration
    in_speed = 4700

    start = transition_duration
    end = dur - transition_duration

    buffer = 50
    max_radius = start*in_speed-buffer
    def circ(rd):
        return circle(screensize=(clip.w,clip.h),
                center=(clip.w/2,clip.h/2),
                radius=rd,
                color=opacity, bg_color=1, blur=4)

    return selector(t, dur, circ(max(0.01,int(in_speed*t-buffer))), circ(max_radius), circ(max(0.01,int(max_radius-in_speed*(t-end)))))

def walrus_movement(clip, t, low, high):
    dur = clip.duration

    dif = low - high
    return selector(t, dur,
                    ('center', min(low, high + dif * x(t))),
                    ('center',low),
                    ('center', max(high, low - dif * y(t, dur))))

def walrus_size(clip, t):
    dur = clip.duration

    return selector(t, dur, min(0.1+x(t), 1), 1, max(1-y(t,dur), 0.1))


def selector(t, dur, start, middle, end):
    if dur < 2:
        # If the duration of the clip is less than 2 seconds,
        # show only the static imagery.
        return middle
    elif t < transition_duration: # First part of animation
        return start
    elif t > dur - transition_duration: # Final part of animation
        return end
    else: # Middle part of animation
        return middle
