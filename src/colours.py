#!/usr/bin/env python3


BLUE = "blue"
TURQOISE = "turqoise"
RED = "red"
POINTS = "points"
NEW = "new"

# TODO: make the colour pallettes easier to access, e.g. a dictionary where
#       you can ask for the color for example MAIN_BG or SECONDARY_BG
colour_pallettes = { TURQOISE: ((33, 47, 60),    # lighter grey
                                  (5, 5, 5),      # darker gray
                                  (142,255,199),   # turqoise
                                  (89,159,124),    # darker turqoise
                                  (255, 199, 142)), # orange

                     BLUE: ((33, 47, 60),    # secondary bg color, lighter grey
                            (5, 5, 5),       # main bg color, darker grey
                            (164,190,255),   # blue
                            (103,119,159),   # darker blue
                            (255, 229, 164),
                            (134, 0, 25)      # very dark red
                            ),

                     # ***********************************
                     #
                     #

                     # base color: (58, 166, 255),  #3aa6ff
                     NEW: (
                            (14, 42, 64),   # main bg color (dark blue)
                            (22, 62, 96),  # secondary bg color (darkish blue)
                            (255,255,255),   # indicator color (white)
                            (51,145,223),   # tertiary_bg_color (lighter blue)
                            (255, 147, 58), # accent_color (orange)
                            (129, 23, 23), # accent_color (red)
                            ),


                     #
                     #
                     # *****************************************************

                     RED: (#(33, 47, 60),      # secondary bg color, lighter grey
                            (27, 96, 85),
                            #(5, 5, 5),        # main bg color, dark grey
                            (128, 37, 51),        # main bg color
                            (217,128,144),     # red
                            (155,72,87),      # darker red
                            (255, 255, 255),
                            (134, 0, 25)      # very dark red
                           ), # white

                    POINTS: (
                            (255, 77, 64),   # red
                            (236, 183, 159), # orange
                            (255, 229, 164), # orange-yellow
                            (64, 218, 150),  # green
                            (64,  132, 218), # blue
                            (33, 194, 255),  # turqoise
                            (73, 64, 218),   # darker blue
                            (218, 64, 209),  # purple
                            (190, 255, 164), # yellow green
                            (136, 232, 189), # light green
                            (136, 233, 250), # light turqoise

                            )


                    }
