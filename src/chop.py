import sys
sys.path.append('~/projects/moviepy/moviepy')

from config import read_config, make_default_config
from files import get_project_files
from hidden_prints import HiddenPrints
from terminal_colors import *
from constants import *
from stats import MutableData
from cut import parse_cuts

from moviepy.audio.fx.all import *
from datetime import datetime, timedelta
import time
import argparse
import os
from moviepy.editor import VideoFileClip, concatenate_videoclips


def process_with_moviepy(filenames, datafiles, outfile,
                         titles, scores, preview, frames, audio_only, dry,
                         ht_short, at_short, conf):

    start = time.time()

    clips = []
    if scores and ht_short and at_short:
        outfile = f"{ht_short}-{at_short}".replace(" ", "").replace("ö", "o").replace("ä", "a").replace("å", "a").lower()
        outfile = f"{outfile}.mp4"
    print(bold(header("\nOutfile:")))
    print(f"{outfile}")

    data = titles
    if scores:
        scoreboard_graphics, players, points, info = scores
        data = MutableData(scoreboard_graphics, players, points, info, titles, True)

    for video_file, data_file in zip(filenames, datafiles):
        video = VideoFileClip(video_file)
        if VIDEO_VOL_FACTOR != 1 and video.audio:
            video.audio = video.audio.multiply_volume(VIDEO_VOL_FACTOR)

        res = parse_cuts(video, data_file, data, dry, preview, frames, conf)
        if res:
            cuts, data = res
        else:
            # Validation failed
            return
        if cuts:
            clips.extend(cuts)

    if preview:
        print(header(bold("\nShowing the preview images.")))
        # 0 = start title
        # 1 = first throw from team 1
        # 5 = first throw from team 2
        # 32 = last throw of the set (team 2)
        # 33 = fst set ending graphic
        # 34 = second set first throw (team 2)
        # 65, last - 1 = last throw of the game (team 1)
        # 66, last = game ending graphic
        last = len(clips)
        interesting_clips = [0, 1, 5, 32, 33, 34, last - 1, last]
        show_all = True

        for i, c in enumerate([clips[0]] + clips):
            with HiddenPrints():
            # for some reason the first clip shown is always black.
            # Always show extra clip at the beginning.
                if i == 0:
                    c.show(4.5)
                elif i + 1 in interesting_clips or show_all:
                    if i == 1:
                        c.show(5, interactive = True)
                        c.show(12, interactive = True)
                    else:
                        c.show(TYPICAL_THROW_LENGTH + 0.5, interactive = True)
        return
    


    print(header(bold("\nStarting the chopping process:")))
    print(f"In time: {datetime.now()}")
    if clips:
        clip1 = clips[0]
        if conf.ad:
            print(cyan(bold("\nAdding an advertisement to the beginning.")))
            ad = VideoFileClip(conf.ad, target_resolution=(clip1.h, clip1.w))
            ad.audio = ad.audio.multiply_volume(AD_VOL_FACTOR)
            clips.insert(0, ad)


        final_clip = concatenate_videoclips(clips)#.set_fps(5) # for faster debug

        if audio_only:
            # Only save the audio (for volume level testing).
            final_clip.audio.write_audiofile("audio.mp3", 44100)
        else:
            # The normal case
            final_clip.write_videofile(outfile, threads=18,
                                       #fps = clip1.fps,
                                       preset="ultrafast",
                                       ffmpeg_params=["-crf", "17"],
                                       #logger=None
                                       )

        print(header(f"\nCreating video took {timedelta(seconds=(time.time() - start))}."))
    else:
        print(warn("\nNo clips to process. Exiting."))

if __name__ == "__main__":
    args = argparse.ArgumentParser()
    args.add_argument("-r", "--recursive", action="store_true", help="Whether to look for input files recursively", required=False)
    args.add_argument("-i", "--input", type=str, metavar="INPUT_NAME", help="Input directory where to look for video files", required=False)
    args.add_argument("-o", "--output", type=str, metavar="OUTPUT_NAME", default="out.mp4", help="Desired name for the output file", required=False)
    args.add_argument("-t", "--titles", type=str, metavar="TITLE_FILE", default="titles", help="Name of the file containing titles for the video", required=False)
    args.add_argument("-s", "--stats", type=str, metavar="STATS_FILE", default="stats", help="Name of the file containing the stats for the video. Overrides titles.", required=False)
    conf_help = "Name of the configuration file. If not given and no file named \"chop.conf\" exists in the current directory, default values will be used"
    args.add_argument("-c", "--config", type=str, metavar="CONFIG_FILE", default="chop.conf", help=conf_help, required=False)
    args.add_argument("-g", "--gen-config", action="store_true", help="Generate configuration file with default values and exit", required=False)
    args.add_argument("-p", "--preview", action="store_true", help="Preview video clip without exporting it", required=False)
    args.add_argument("-f", "--frames", type=int, metavar="FRAMES", help="How many cuts to make", required=False)
    args.add_argument("-d", "--dry-run", action="store_true", help="Only scan and display the files to be cut but don't start chopping them")
    args.add_argument("-a", "--audio-only", action="store_true", help="Only record the audio of the cut video.")
    args = args.parse_args()

    if args.gen_config:
        make_default_config(args.config)
        exit()

    conf = read_config(args.config)

    
    input_dir = args.input

    if not args.input:
        # If no input dir is given, use the working directory (where this script is ran)
        input_dir = os.getcwd()
    

    print(header(bold("Looking for files in :")))
    print(input_dir)

    videofiles, datafiles, titles, scores = \
        get_project_files(input_dir, args.titles, args.stats, args.recursive, conf)


    print(header(bold("Chopping following files:")))
    for v in videofiles:
        print(v)

    print(header(bold("\nUsing following titles:")))
    for t in titles:
        print()
        for x in t.split("\\n"):
            print(x.strip())

    ht_short = ""
    at_short = ""
    if (scores and scores[0]):
        print(header(bold("\nUsing following stats:\n")))
        print(warn("Showing only first and last titles to save space"))

        for p, (rh1, sh1, ra1, sa1, \
                rh2, sh2, ra2, \
                sa2, totH, totA, ht_short, \
                at_short, throwing \
                ) in [list(zip(scores[1], scores[0]))[i] for i in (0, -1)]:

            plr_text = p
            if not p:
                plr_text = ""
            print()
            print(f"{ht_short} {rh1} {sh1} | {rh2} {sh2} | {totH}       {plr_text:>10}")
            print(f"{at_short} {ra1} {sa1} | {ra2} {sa2} | {totA}")

    process_with_moviepy(videofiles, datafiles, args.output, titles,
                         scores, args.preview, args.frames, args.audio_only,
                         args.dry_run, ht_short, at_short, conf)
