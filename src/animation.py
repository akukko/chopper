from walrus_animation import *
from constants import *

from moviepy.audio.fx.all import *

def up_and_stay(t, clip_height, dur, object_height,
                     summit_height=None, start_time=None):
    if start_time != None and (dur - start_time < 2):
        raise Exception(f"Last throw of the set was too short for animation: {start_time} {dur}")

    if not summit_height:
        summit_height = clip_height - object_height - 20

    if start_time:
        dur = dur - start_time
        t = max(0, t - start_time)

    transition_duration = 0.25
    start_pad_duration = 0.1

    travel_distance = clip_height - summit_height

    if t < start_pad_duration:
        # Start the title animation slighty after the clip begins, because it looks better.
        return clip_height # Right below visible screen
    elif t < transition_duration + start_pad_duration:
        return clip_height - travel_distance * \
            ((t - start_pad_duration) / (transition_duration))

    return summit_height


def up_up_movement(t, clip_height, dur, object_height,
                     summit_height=None, start_time=None):

    if not summit_height:
        summit_height = clip_height - object_height - 20

    if start_time:
        dur = dur - start_time
        t = max(0, t - start_time)

    if dur < 2:
        return summit_height

    # Travel at a constant speed so graphics look nice
    travel_speed = 3000
    travel_distance = clip_height - summit_height

    slowest_transition_duration = clip_height / travel_speed
    transition_duration = travel_distance / travel_speed


    start_pad_duration = 0.1
    end_pad_duration = 0.5

    # Finish the title animation slightly before the clip ends, because it
    # looks cleaner that way when two titles are back-to-back or when the
    # video ends to a title.


    transition_difference = \
        slowest_transition_duration - transition_duration + start_pad_duration
    t = max(0, t - transition_difference)
    #final = dur - transition_duration - end_pad_duration
    final = dur - end_pad_duration - transition_difference

    #if t < start_pad_duration or t < transition_difference:
    if t == 0:
        # Start the title animation slighty after the clip
        # begins, because it looks better.

        # Also start the animation later if this clip doesn't need to
        # travel the full distance.
        return clip_height # Right below visible screen
    elif t < transition_duration + start_pad_duration:
        # moving from bottom to middle
        return clip_height - travel_distance * \
            ((t - start_pad_duration) / (transition_duration))
    elif t > final:
        # moving from middle to top
        return summit_height - \
            travel_distance * ((t - final) / transition_duration)

    return summit_height

def up_down_movement(t, clip_height, dur, object_height,
                     summit_height=None, start_time=None):

    if not summit_height:
        summit_height = clip_height - object_height - 20

    if start_time:
        dur = dur - start_time
        t = max(0, t - start_time)

    if dur < 2:
        return summit_height

    transition_duration = 0.25
    start_pad_duration = 0.1
    end_pad_duration = 0.2

    # Finish the title animation slightly before the clip ends, because it
    # looks cleaner that way when two titles are back-to-back or when the
    # video ends to a title.
    final = dur - transition_duration - end_pad_duration

    travel_distance = clip_height - summit_height

    if t < start_pad_duration:
        # Start the title animation slighty after the clip begins, because it looks better.
        return clip_height # Right below visible screen
    elif t < transition_duration + start_pad_duration:
        return clip_height - travel_distance * \
            ((t - start_pad_duration) / (transition_duration))
    elif t > final:
        return summit_height + \
            travel_distance * ((t - final) / transition_duration)

    return summit_height
