"""
An alternative text clip for Moviepy, relying on Gizeh instead of ImageMagick
Advantages:
- Super fast (20x faster)
- no need to install imagemagick
- full-vector graphic, no aliasing problems
- Easier font names
Disadvantages:
- Requires Cairo installed
- Doesnt support kerning (=letters spacing)
"""


import sys
import numpy as np
import cairocffi as cairo

try:
    import gizeh as gz
    GIZEH_AVAILABLE = True
except ImportError:
    GIZEH_AVAILABLE = False
import numpy as np
from moviepy.editor import ImageClip

def autocrop(np_img):
    """Return the numpy image without empty margins."""
    if len(np_img.shape) == 3:
        if np_img.shape[2] == 4:
            thresholded_img = np_img[:,:,3] # use the mask
        else:
            thresholded_img = np_img.max(axis=2) # black margins
    zone_x = thresholded_img.max(axis=0).nonzero()[0]
    xmin, xmax = zone_x[0], zone_x[-1]
    zone_y = thresholded_img.max(axis=1).nonzero()[0]
    ymin, ymax = zone_y[0], zone_y[-1]
    return np_img[ymin:ymax+1, xmin:xmax+1]

def gizeh_text(text, font, align='left',
              font_weight='normal', font_slant='normal',
              font_size = 70, font_width = None,
              interline= None, fill_color=(0,0,0),
              stroke_color=(0, 0, 0), stroke_width=2,
              bg_color=None):
    """Return an ImageClip displaying a text.

    Parameters
    ----------

    text
      Any text, possibly multiline

    font_family
      For instance 'Impact', 'Courier', whatever is installed
      on your machine.

    align
      Text alignment, either 'left', 'center', or 'right'.

    font_weight
      Either 'normal' or 'bold'.

    font_slant
      Either 'normal' or 'oblique'.

    font_height
      Eight of the font in pixels.

    font_width
      Maximal width of a character. This is only used to
      create a surface large enough for the text. By
      default it is equal to font_height. Increase this value
      if your text appears cropped horizontally.

    interline
      number of pixels between two lines. By default it will be

    stroke_width
      Width of the letters' stroke in pixels.

    stroke_color
      For instance (0,0,0) for black stroke or (255,255,255)
      for white.

    fill_color=(0,0,0),
      For instance (0,0,0) for black letters or (255,255,255)
      for white.

    bg_color
      The background color in RGB or RGBA, e.g. (255,100,230)
      (255,100,230, 128) for semi-transparent. If left to none,
      the background is fully transparent

    """

    if not GIZEH_AVAILABLE:
        raise ImportError("`text_clip` requires Gizeh installed.")

    stroke_color = np.array(stroke_color)/255.0
    fill_color = np.array(fill_color)/255.0
    if bg_color is not None:
        np.array(bg_color)/255.0

    if font_width is None:
        font_width = font_size
    if interline is None:
        interline = 0.3 * font_size
    line_height = font_size + interline
    lines = text.splitlines()
    max_line = max(len(l) for l in lines)
    W = int(max_line * font_width + 2 * stroke_width)
    H = int(len(lines) * line_height + 2 * stroke_width)
    surface = gz.Surface(width=W, height=H, bg_color=bg_color)
    xpoint = {
        'center': W/2,
        'left': stroke_width + 1,
        'right': W - stroke_width - 1
    }[align]
    for i, line in enumerate(lines):
        ypoint = (i+1) * line_height
        text_element = make_text(line, fontfamily=font, fontsize=font_size,
                               h_align=align, v_align='top',
                               xy=[xpoint, ypoint], fontslant=font_slant,
                               stroke=stroke_color, stroke_width=stroke_width,
                               fill=fill_color)
        text_element.draw(surface)
    cropped_img = autocrop(surface.get_npimage(transparent=True))
    return ImageClip(cropped_img)





def _set_source(ctx, src):
    """ Sets a source before drawing an element.
    The source is what fills an element (or the element's contour).
    If can be of many forms. See the documentation of shape_element for more
    details.
    """
    if len(src) == 4:  # RGBA
        ctx.set_source_rgba(*src)
    else:  # RGB
        ctx.set_source_rgb(*src)

def make_text(txt, fontfamily, fontsize, fill=(0, 0, 0),
         h_align="center", v_align="center",
         stroke=(0, 0, 0), stroke_width=0,
         fontweight="normal", fontslant="normal",
         angle=0, xy=[0, 0], y_origin="top"):
    """Create a text object.
    Parameterps
    -----------
    v_align
      vertical alignment of the text: "top", "center", "bottom"
    h_align
      horizontal alignment of the text: "left", "center", "right"
    fontweight
      "normal" "bold"
    fontslant
      "normal" "oblique" "italic"
    y_origin
      Adapts the vertical orientation of the text to the coordinates system:
      if you are going to export the image with y_origin="bottom" (see for
      instance Surface.write_to_png) then set y_origin to "bottom" here too.
    angle, xy, stroke, stroke_width
      see the doc for ``shape_element``
    """

    fontweight = {"normal": cairo.FONT_WEIGHT_NORMAL,
                  "bold":   cairo.FONT_WEIGHT_BOLD}[fontweight]
    fontslant = {"normal":  cairo.FONT_SLANT_NORMAL,
                 "oblique": cairo.FONT_SLANT_OBLIQUE,
                 "italic":  cairo.FONT_SLANT_ITALIC}[fontslant]

    def draw(ctx):

        ctx.select_font_face(fontfamily, fontslant, fontweight)
        ctx.set_font_size(fontsize)
        xbear, ybear, w, h, xadvance, yadvance = ctx.text_extents(txt)
        xshift = {"left": 0, "center": -w / 2, "right": -w}[h_align] - xbear
        yshift = {"top": 0, "center": -h / 2, "bottom": -h}[v_align] - ybear
        new_xy = np.array(xy) + np.array([xshift, yshift - fontsize])

        if stroke_width > 0:
            ctx.move_to(*new_xy)
            ctx.text_path(txt)
            _set_source(ctx, stroke)
            ctx.set_line_width(stroke_width)
            ctx.stroke()

        ctx.move_to(*new_xy)
        ctx.text_path(txt)
        _set_source(ctx, fill)
        ctx.fill()

    return (gz.Element(draw).scale(1, 1 if (y_origin == "top") else -1)
            .rotate(angle))
