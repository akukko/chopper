
MAX_TEAM_NAME_LENGTH = 20
MAX_SHORT_NAME_LENGTH = 6
VIDEO_VOL_FACTOR = 1
INTRO_OUTRO_AUDIO_VOL_FACTOR = 0.5
AD_VOL_FACTOR = 1
# in seconds
TYPICAL_THROW_LENGTH = 4.5

# 1 for FHD, 2 for 4K
RES_FACTOR = 1

SFX_ROCKSTAR = "/sfx/rockstar-trailer.mp3"
SFX_SIPULI = "/sfx/sipulisi_pettaa.ogg"
SFX_LAIDALLA = "/sfx/laidalla_liikehdintaa.ogg"
SFX_HIIVATTI = "/sfx/hiivatti1.ogg"
SFX_PASKA = "/sfx/paska2.ogg"
SFX_CHILL = "/sfx/chill-abstract-intention.mp3"

IMG_TAMMER = "tammer.png"
IMG_TAMMER_SMALL = "tammer-pieni.png"
IMG_PYTTY = "pytty.png"
