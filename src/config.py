from terminal_colors import *
from files import get_lines
from constants import *
from colours import BLUE, RED, NEW

TITLE_FONT_KEY = "starttitlefont"
TITLE_SIZE_KEY = "titlesize"
TITLE_COLOR_KEY = "titlecolor"
VOL_FACTOR_KEY = "volfactor"
SPEED_FACTOR_KEY = "slowfactor"
TITLE_ANIM_KEY = "titleanim"
AD = "ad"




class Configuration:
    def __init__(self):
        #inconsolata = "Inconsolata-Semi-Condensed-ExtraBold"
        inconsolata = "Inconsolata-Bold"
        roboto_mono = "RobotoMono-Nerd-Font"

        # This is shown in start title and end title
        # Has to be monospace so that end screen table looks right.
        # self.starttitlefont = "RobotoMono-Nerd-Font"
        self.starttitlefont = roboto_mono
        # self.starttitlefont = "Roboto-Regular"#"Barlow-Semi-Condensed-ExtraLight"
        # This is used for start title
        self.titlesize = RES_FACTOR * 100
        self.playerfont = "Rockwell"
        self.playersize = RES_FACTOR * 60
        # This is used for the middle titles and final result screen
        self.scoresize = RES_FACTOR * 70
        # this is used for middle of the game titles also
        self.endtitlefont = inconsolata
        self.scoreboardfont = inconsolata
        self.scoreboardsize = RES_FACTOR * 35
        self.pointsfont = "RobotoMono-Nerd-Font-Bd"
        #self.throwfont = "Roboto-Regular"
        self.throwfont = roboto_mono
        self.namefont = "Roboto-Medium"
        # Used for the score number in scoreboard
        self.scorefont = "Redwing-Bold"
        # DeepSkyBlue is the Pariliiga color
        # crimson is a good basic red
        self.titlecolor = "White" #"DeepSkyBlue" #"crimson"
        self.lightscorefontcolor = "orange"#"khaki"
        self.volfactor = 0.2
        self.speedfactor = 0.25
        # Options are "walrus", "upwards" and "up-down"
        self.titleanim = "walrus"
        self.titleimg = IMG_TAMMER #IMG_PYTTY #"kyykkamursu.png"
        self.pallette = NEW
        self.ad = None #"/home/n/videos/futu.mp4"
        self.starting_score = -80
        self.throw_amount = 64
        self.team_concurrent_throws = 8 # The name of this should be consequent
        self.comp_name = "Joukkue-SM 2022"#"Futuliiga 2022"

    def __str__(self):
        key_vals = { TITLE_FONT_KEY: self.starttitlefont
                   , TITLE_SIZE_KEY: self.titlesize
                   , TITLE_COLOR_KEY: self.titlecolor
                   , VOL_FACTOR_KEY: self.volfactor
                   , SPEED_FACTOR_KEY: self.speedfactor
                   , TITLE_ANIM_KEY: self.titleanim
                   , AD : self.ad
                   }

        conf_str = ""
        for key, val in key_vals.items():
            conf_str += f"{key:<32}{val}\n"
        return conf_str

def make_default_config(configfile):
    lines = get_lines(configfile)
    if lines:
        print(warn("Requested creation of default configuration file, but configuration file already exists. Not overwriting."))
        return

    conf = Configuration()
    with open(configfile, "w") as f:
        f.write(str(conf))
    print(ok(f"Created new configuration file with default values at: {header(configfile)}"))

def read_config(configfile):
    # Create default configuration
    conf = Configuration()

    lines = get_lines(configfile)

    if lines:
        return handle_config_values(conf, lines)
    else:
        # Config was empty or didn't exist
        print(bold(cyan("Configuration file didn't exist or was empty. Using default values.\n")))

    return conf

def handle_config_values(conf, lines):
    for l in lines:
        vals = l.split()
        if len(vals) != 2:
            print(warn(f"\nIncorrect configuration syntax for line:\n    \"{l.strip()}\".\nExactly two values\
 are allowed per line, separated by whitespace. Ignoring line.\n"))
            continue

        key, value = vals
        key_val_text = header(bold(f"[{key}: {value}]"))
        print(f"Read configuration value: {key_val_text}")
        if key == TITLE_FONT_KEY:
            conf.starttitlefont = value
        elif key == TITLE_COLOR_KEY:
            conf.titlecolor = value
        elif key == TITLE_SIZE_KEY:
            conf.titlesize = int(value)
        elif key == VOL_FACTOR_KEY:
            conf.volfactor = float(value)
        elif key == SPEED_FACTOR_KEY:
            conf.speedfactor = float(value)
        elif key == TITLE_ANIM_KEY:
            conf.titleanim = value
        elif key == AD:
            if value.lower() in ["none", "null"]:
                conf.ad = None
            else:
                conf.ad = value
        else:
            print(warn(f"\nConfiguration key not recognized, ignoring it: {header(bold(key))} \n"))
    print(ok("\nConfiguration read succesfully.\n"))
    return conf

