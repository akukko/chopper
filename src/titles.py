from colours import *
from gizeh_text_clip import gizeh_text
from animation import up_and_stay, up_up_movement, up_down_movement
from constants import RES_FACTOR

from moviepy.editor import TextClip, CompositeVideoClip, \
    ImageClip, AudioFileClip, CompositeAudioClip, \
    concatenate_videoclips, ColorClip

from PIL import Image
from moviepy.audio.fx.all import *

import os

black = "black"

transition_duration = 0.45

final_value = 1

def enlarge(t):
    # This should peak at around 1.4x and stabilize at 1x at time 0.45
    def f(x):
        return -69.2774 * x**3 + 37.6350 * x**2 - 0.6963 * x + 0.0052

    return max(0.05, f(t))

def contract(t, duration):
    return enlarge(duration - t)

def gradual_appearance(dur, t):
    res = selector(t, dur, enlarge(t), final_value, contract(t, dur))
    return res


def selector(t, dur, start, middle, end):
    if t < transition_duration: # First part of animation
        return start
    elif t > dur - transition_duration: # Final part of animation
        return end
    else: # Middle part of animation
        return middle

def draw_start_title(clip, info, home, away, conf):
    start_title_min_length = 14
    if clip.duration <= start_title_min_length:
        raise Exception("Start title from information needs to "
                       + f"be longer than {start_title_min_length} seconds")

    (_, comp_name, field_name, date) = info


    start_wait = 0.1
    start = clip.subclip(0, start_wait)

    # Main title screen

    title_time = 7
    title_clip = clip.subclip(start_wait, title_time)

    main_titles = three_titles(title_clip,
                               (comp_name, RES_FACTOR * 60),
                               (field_name, RES_FACTOR * 50),
                               (f"{date}", RES_FACTOR * 40),
                               conf)

    # Players
    player_clip = clip.subclip(title_time, clip.duration)
    plr_titles = two_players(player_clip,
                              ("VASTAKKAIN", RES_FACTOR * 80),
                              (home, RES_FACTOR * 50),
                              (away, RES_FACTOR * 50),
                              conf)

    comp_clip = concatenate_videoclips([start, main_titles, plr_titles])
    comp_clip.duration = clip.duration

    return comp_clip

def three_titles(bg_clip, middle, bottom, top, conf):
    elements = middle_bottom_top(middle, bottom, top, bg_clip, conf)
    titles = CompositeVideoClip([bg_clip] + elements)
    titles.duration = bg_clip.duration
    return titles

def two_players(bg_clip, middle, top, bottom, conf):
    elements = middle_top_bottom(middle, top, bottom, bg_clip, conf)
    titles = CompositeVideoClip([bg_clip] + elements)
    titles.duration = bg_clip.duration
    return titles

def get_composition(clip, element):
    composition = CompositeVideoClip([clip, element])
    composition.duration = clip.duration
    return composition


def middle_bottom_top(middle_text, bottom_text, top_text, clip, conf):
    '''
    Show the given titles so that the middle title is expanded first,
    the bottom title second and the top title third.

    The third title is shown much later than others.

    The timings of the titles are matches to SFX_CHILL

    +--------------------+
    |    top_text        |   third to appear
    +--------------------+
    |    middle_text     |   first to appear
    +--------------------+
    |    bottom_text     |   second to appear
    +--------------------+
    '''
    (main_bg_color, secondary_bg_color,
     indicator_color, tertiary_bg_color,
     accent_color, active_bg_color) = colour_pallettes[conf.pallette]

    (mt, m_size) = middle_text
    (bt, b_size) = bottom_text
    (tt, t_size) = top_text

    # Constant size title elements (colored boxes with text in them)
    mid = draw_title_element(mt.upper(), conf, secondary_bg_color, m_size)
    bot = draw_title_element(bt, conf, tertiary_bg_color, b_size)
    top = draw_title_element(tt, conf, accent_color, t_size)

    width = max(mid.w, bot.w, top.w)
    mid = draw_title_element(mt.upper(), conf, secondary_bg_color, m_size, width)
    bot = draw_title_element(bt, conf, main_bg_color, b_size, width)
    top = draw_title_element(tt, conf, tertiary_bg_color, t_size, width)

    elements = []

    # top first to hide it behing others
    elements.append(expand(BOUNCE_UPWARDS, clip, top, - mid.h / 2, 3.9))
    # bot second to hide it behind middle
    elements.append(expand(DOWNWARDS, clip, bot, (mid.h / 2), 1.6))
    elements.append(expand(OUTWARDS, clip, mid))

    return elements

def middle_top_bottom(middle_text, top_text, bottom_text, clip, conf):
    # TODO: combine this with the above function
    (main_bg_color, secondary_bg_color,
     indicator_color, tertiary_bg_color,
     accent_color, accent_two) = colour_pallettes[conf.pallette]

    (mt, m_size) = middle_text
    (tt, t_size) = top_text
    (bt, b_size) = bottom_text

    # Constant size title elements (colored boxes with text in them)
    mid = draw_title_element(mt.upper(), conf, accent_color, m_size)
    top = draw_title_element(tt, conf, secondary_bg_color, t_size)
    bot = draw_title_element(bt, conf, main_bg_color, b_size)

    width = max(mid.w, bot.w, top.w)
    mid = draw_title_element(mt.upper(), conf, accent_two, m_size, width)
    top = draw_title_element(tt, conf, secondary_bg_color, t_size, width)
    bot = draw_title_element(bt, conf, main_bg_color, b_size, width)

    elements = []

    # last to first so the later elements are hidden behind ealier
    elements.append(expand(DOWNWARDS, clip, bot, (mid.h / 2), 2.6))
    elements.append(expand(UPWARDS, clip, top, - mid.h / 2, 1.1))
    elements.append(expand(OUTWARDS, clip, mid))

    return elements



def draw_text(text, font, font_color, font_size):
    return TextClip(f"{text}", font_size=font_size, font=font, color=font_color)

def apply_background(clip, bg_color, x_pad, y_pad):
    return clip.on_color((clip.w + x_pad, clip.h + y_pad), bg_color)

def draw_title_element(text, conf, bg_color, font_size, width=None):
    text_clip = draw_text(text, conf.starttitlefont,
                          conf.titlecolor, font_size)
    pad = font_size
    x_pad = pad
    if width != None:
        x_pad = (width - text_clip.w)
    return apply_background(text_clip, bg_color, x_pad, pad)


def get_size_in_time(time, c):
    ct = time - c.start  # clip time
    # GET IMAGE AND MASK IF ANY
    img = c.get_frame(ct).astype("uint8")
    im_img = Image.fromarray(img)
    return im_img.size

def outwards(t, c, frame):
    w, h = get_size_in_time(t, c)
    return ((frame.w - w) / 2, (frame.h - h) / 2)

def downwards(t, c, frame, full_size_clip, y_pad):
    w, _ = get_size_in_time(t, c)
    return ((frame.w - w) / 2, frame.h / 2 + y_pad)

def upwards(t, c, frame, y_pad):
    # The clip c grows larger in time (gradual_appearance function).
    # The position that we get here matches with the movement,
    # so that it looks like the clip object is staying in place while
    # slowly growing upwards.
    #
    # However, in the case of BOUNCE_UPWARDS, the size is later set to be
    # constant, so only the movement remains. Now the clip looks like it bounces
    # upwards, as opposed to staying in place and growing.
    w, h = get_size_in_time(t, c)
    return ((frame.w - w) / 2, frame.h / 2 - h + y_pad)

def expand_with_wait(expand_type, target, frame, full, y_pad, wait):
    def hide_or_expand(t, e_type, target, frame, full, y_pad, wait):
        time = max(t - wait, 0)
        if time == 0:
            # hide image outside of frame
            return (frame.w, frame.h)
        elif e_type in [UPWARDS, BOUNCE_UPWARDS]:
            return upwards(t, target, frame, y_pad)
        elif e_type == DOWNWARDS:
            return downwards(t, target, frame, full, y_pad)
        else:
            return outwards(t, target, frame)

    with_pos = target.with_position(lambda t:
                                    hide_or_expand(t, expand_type, target,
                                                   frame, full, y_pad, wait))
    # This is madness.
    #
    # The initial resizing done by gradual appearance function is used in combination
    # with upwards function to get a movement pattern for a clip that matches
    # the gradual resizing.
    #
    # Then the title is set to a constant size to just have the movement without
    # the expanding.
    #
    # This mismatch between the gradual resizing and upwards position change
    # was originally a bug, but it looked nice so I kept it in as a feature.
    if expand_type == BOUNCE_UPWARDS:
        return with_pos.resize(lambda _: full.size)
    else:
        return with_pos


DOWNWARDS = "downwards"
OUTWARDS = "outwards"
UPWARDS = "upwards"
BOUNCE_UPWARDS = "bounce-up"

def expand(expand_type, frame, clip, y_pad=0, wait=0.0):
    if frame.duration == None:
        raise Exception(f"expand needs frame.duration set")

    dur = frame.duration - wait
    def grad_app(t, dur, wait):
        return gradual_appearance(dur, max(t - wait, 0))

    def grad_app_constant_width(t, clip, dur, wait):
        return (clip.w, clip.h * gradual_appearance(dur, max(t - wait, 0)))

    if expand_type in [UPWARDS, DOWNWARDS, BOUNCE_UPWARDS]:
        t_clip = clip.resize(lambda t: grad_app_constant_width(t, clip, dur, wait))
    else:
        t_clip = clip.resize(lambda t: grad_app(t, dur, wait))

    t_clip.duration = frame.duration
    return expand_with_wait(expand_type, t_clip, frame, clip, y_pad, wait)

side_padding = 12
indicator_width = 10
def draw_scoreboards(clip, roundFst1, fst1, roundFst2, fst2, roundSnd1,
             snd1, roundSnd2, snd2, total1, total2, t1_name, t2_name,
             fst_team_is_throwing, conf):

    (inner_bg_color, scoreboard_bg_color,
     indicator_color, old_set_color,
     total_score_color,
     active_bg_color) = colour_pallettes[conf.pallette]

    t1_name = t1_name[:MAX_SHORT_NAME_LENGTH]
    t2_name = t2_name[:MAX_SHORT_NAME_LENGTH]

    w,h = clip.size

    sb_x = 0.009 * w
    sb_y = 0.016 * h

    def round_on_color(text1, text2, bg_color, x_pos, conf,
                       font_color=conf.titlecolor):

        return text_on_color(text1, text2, bg_color, x_pos, conf,
                              smaller_bg_color=bg_color,
                              font_color=font_color,
                             pad = (side_padding // 4))

    def score_on_color(text1, text2, bg_color, x_pos, conf,
                       smaller_bg_color=indicator_color,
                       font_color=black, pad = (side_padding // 2)):
        return text_on_color(text1, text2, bg_color, x_pos, conf, pad,
                             smaller_bg_color=smaller_bg_color,
                             font_color=font_color,
                             fontsize = conf.scoreboardsize - 10,
                             font=conf.scorefont, strip=True
                             )


    def text_on_color(text1, text2, bg_color, x_pos, conf, pad,
                      smaller_bg_color=indicator_color,
                      font_color=black,
                      fontsize=conf.scoreboardsize - 3,
                      font=conf.scoreboardfont, strip = False):

        if bg_color == smaller_bg_color:
            pad = 0
        smaller_pad = (pad // 2)

        # make bg to get dimensions
        max_text = max(text1, text2, key=len)
        bg_text = "-88" if max_text.strip().lstrip("-").strip().isnumeric() \
            else max_text
        bg = scoreboard_on_color(bg_text, bg_text, bg_color,
                                 x_pos, conf, padding=pad)

        def score_clip(text1, text2, fontsize = conf.scoreboardsize - 4,
                       font=conf.scoreboardfont):

            return TextClip(f"{text1}\n{text2}", font_size=fontsize,
                        font=font,
                        color=font_color)

        smaller_bg = score_clip(bg_text, bg_text)

        if strip:
            text1 = text1.strip()
            text2 = text2.strip()

        text_clip = score_clip(text1, text2, fontsize=fontsize, font=font)

        text = text_clip.on_color(size=(text_clip.w, text_clip.h),
                                  color=smaller_bg_color)
        if bg_color != smaller_bg_color:
            text = text.on_color(
                size=(smaller_bg.w + smaller_pad, smaller_bg.h),
                color=smaller_bg_color)

        text = text.on_color(size=(bg.w, bg.h), color=bg_color)

        final = text.with_position((x_pos, sb_y))
        text_clip.close()
        text.close()

        return final


    def scoreboard_on_color(text1, text2, bg_color, x_pos, conf,
                            font_color=conf.titlecolor, padding=side_padding):

        text_clip = TextClip(f"{text1}\n{text2}", font_size=conf.scoreboardsize,
                        font=conf.scoreboardfont,
                        color=font_color)

        text = text_clip.on_color(size=(text_clip.w + padding, text_clip.h),
                                  color=bg_color)

        final = text.with_position((x_pos, sb_y))
        text_clip.close()
        text.close()
        return final

    sb_x_in = sb_x + indicator_width

    # Show team names

    team_names = text_on_color(t1_name, t2_name, scoreboard_bg_color,
                               sb_x_in, conf, smaller_bg_color=inner_bg_color,
                               pad=side_padding * 2,
                               font_color=conf.titlecolor)

    sb_x_in += team_names.w - side_padding // 2


    snd_set_results_exist = snd1 != None and snd2 != None
    fst_set_score_bg = indicator_color
    fst_set_score_color = black
    fst_set_text_color = conf.titlecolor

    if snd_set_results_exist:
        fst_set_score_bg = old_set_color
        fst_set_text_color = old_set_text_color

    # These are not shown for singles game, because the throw number
    # is either same as amount of karttus used, or 20.
    fst_set_rounds = []
    if roundFst1 and roundFst2:
        # Show the current round and throw number in first set
        fst_set_rounds = round_on_color(roundFst1, roundFst2,
                                        scoreboard_bg_color, sb_x_in, conf,
                                        font_color = fst_set_text_color)
        sb_x_in += fst_set_rounds.w
        fst_set_rounds = [fst_set_rounds]


    # Show current points difference in first set


    fst_set_scores = score_on_color(fst1, fst2, scoreboard_bg_color,
                                    sb_x_in, conf,
                                    smaller_bg_color=fst_set_score_bg,
                                    font_color=fst_set_score_color)
    sb_x_in += fst_set_scores.w

    # Why is the 1 needed here? Does it depend on the text.h?
    i_pad_y = side_padding
    indicator_height_modifier = 0 if fst_team_is_throwing else (team_names.h // 2) + 1

    i_pad_x = 3
    i_size = (indicator_width-i_pad_x, (team_names.h//2 - i_pad_y))
    indicator = ColorClip(size=i_size, color=indicator_color)
    indicator = indicator.with_position(
        (sb_x + (i_pad_x // 2) + (side_padding // 2),
         sb_y + (i_pad_y // 2) + indicator_height_modifier))

    indicator_bg = ColorClip(size=(indicator_width, team_names.h), color=scoreboard_bg_color)
    indicator_bg = indicator_bg.with_position((sb_x, sb_y))


    def try_get_snd_and_total_scoreboards():
        if not snd_set_results_exist:
            return []

        snd_set_rounds = round_on_color(roundSnd1, roundSnd2, scoreboard_bg_color, sb_x_in, conf)
        snd_set_scores = score_on_color(snd1, snd2, scoreboard_bg_color,
                                             (sb_x_in + snd_set_rounds.w), conf)

        if not total1 or not total2:
            return [snd_set_rounds, snd_set_scores]

        total_x = sb_x_in + snd_set_rounds.w + snd_set_scores.w + side_padding
        total_scores = score_on_color(total1, total2, scoreboard_bg_color, total_x,
                                    conf, smaller_bg_color=total_score_color,
                                    pad=side_padding)

        return [snd_set_rounds, snd_set_scores, total_scores]

    new_clip = ([team_names] + fst_set_rounds + [fst_set_scores] +
                try_get_snd_and_total_scoreboards() +
                [indicator_bg, indicator])

    return new_clip

def build_scoreboard(ordered_stats, throw_amount, scores, fst1,
                     fst2, snd1, snd2, conf):
    t1_name = scores.ht_short
    t2_name = scores.at_short

    # ---- ------------------ ---- #
    # ---- points calculation ---- #
    # ---- ------------------ ---- #

    def ceil(a, b):
        return -1 * (-a // b)

    def t1_throwing(throw_amount, conf):
        set_1 = throw_amount <= conf.throw_amount // 2
        fst_team_throwing = ceil(throw_amount, 4) % 2 != 0
        return set_1 and fst_team_throwing or not set_1 and not fst_team_throwing

    def throw_num(t_amount):
        set_1 = t_amount <= conf.throw_amount // 2
        x = ((ceil(t_amount, conf.team_concurrent_throws) % 2))
        t_amt = t_amount if set_1 else t_amount - (conf.throw_amount // 2)
        y = ((ceil(t_amt, conf.team_concurrent_throws * 2)) - x)
        return t_amt - y * conf.team_concurrent_throws

    def pts(score, p_out, t_amt):
        score += p_out
        if score == 0:
            t_num = throw_num(t_amt)
            return conf.throw_amount // 2 // 2 - t_num
        return score

    def fst_or_snd_pts(f, s, p_out, t_amt):
        if not s:
            f = pts(f, p_out, t_amt)
        else:
            s = pts(s, p_out, t_amt)
        return (f, s)

    # ---- --------------- ---- #
    # ---- text clip stuff ---- #
    # ---- --------------- ---- #

    # ------------- #
    # Update scores #
    # ------------- #

    throw = ordered_stats.pop(0)

    # Handle case of field emptying with less than the max amount of throws
    while throw == "E":
        throw = ordered_stats.pop(0)
        throw_amount += 1

    # If max amount of throws per set have been thrown, reset scores for snd set
    if throw_amount == (conf.throw_amount // 2 + 1):
        snd1 = conf.starting_score
        snd2 = conf.starting_score

    # At this point the throw should either be a H or a number
    if throw == "H":
        points_out = 0
    else:
        points_out = int(throw)

    fst_team_is_throwing = t1_throwing(throw_amount, conf)
    total1 = fst1 + snd1 if fst1 and snd1 else None
    total2 = fst2 + snd2 if fst2 and snd2 else None
    params = (None, fst1, None, fst2, None, snd1, None, snd2, total1, total2, t1_name, t2_name, fst_team_is_throwing)

    if fst_team_is_throwing:
        fst1, snd1 = fst_or_snd_pts(fst1, snd1, points_out, throw_amount)
    else:
        fst2, snd2 = fst_or_snd_pts(fst2, snd2, points_out, throw_amount)

    # Increment throw_amount only after all the point calculation is done
    throw_amount += 1

    return params, ordered_stats, throw_amount, fst1, fst2, snd1, snd2


def build_title(vid, title_start, title_end, *args, **kwargs):
    clip = vid.subclip(title_start, title_end)
    return add_title_to_clip(clip, *args, **kwargs)

def add_title_to_clip(clip, t, font, fontsize, conf,
                          start_title=False, fadeout_title=False,
                            scoreboards=[]):
    if t:
        t = t.replace('\\n', '\n')
    dur = clip.duration


    if start_title:
        # Uses ImageMagick, which looks like ass. But it centers multiline
        # text properly so use it anyway.
        text = TextClip(f"{t}", font_size=fontsize, font=font,
                        color=conf.titlecolor,
                        stroke_width=3, stroke_color="black")
    elif fadeout_title:
        # This is a hack. Gizeh uses different font names and the
        # inconsolata font name used for these titles happens to match
        # the one used by gizeh when you replace the dashes with space.
        #
        # It happens to work for Inconsolata fonts
        #
        # The same doesn't work for all fonts, so this would need a
        # better solution at some point. Maybe python will figure out
        # how to do font names in a sane way at some point.
        font = font.replace("-", " ")
        text = gizeh_text(f"{t}", font_size=fontsize,
                          font=font, fill_color=(255,255,255))
    else:
        # Same hack as above.
        font = font.replace("-", " ")
        # The custom gizeh TextClip has nicer stroking, so use that for
        # texts that are either singleline or don't need to be centered
        text = gizeh_text(f"{t}", font_size=fontsize,
                          font=font, fill_color=(255,255,255),
                          stroke_width=9, stroke_color=(0,0,0))


    w,h = clip.size



    def upwards_movement(t, h, clip):
        summit_height = h - (9/10) * clip.h

        transition_duration = 0.25
        start_pad_duration = 0.15
        end_pad_duration = 0.15
        stable_duration = \
            dur - 2 * transition_duration - start_pad_duration - end_pad_duration

        # Finish the title animation slightly before the clip ends, because it
        # looks cleaner that way when two titles are back-to-back or when the
        # video ends to a title.
        final_start = dur - transition_duration - end_pad_duration

        travel_distance = h - summit_height
        stable_travel_distance = travel_distance / 2.5
        final_travel_distance = h - travel_distance - stable_travel_distance


        if dur < 2:
            # If the clip is too short, there's no point
            # showing animations since the text will be unreadable.
            return summit_height
        elif t < start_pad_duration:
            # Start the title animation slighty after the clip
            # begins, because it looks better.
            return h
        elif t < transition_duration + start_pad_duration:
            return h - travel_distance * ((t - start_pad_duration) / transition_duration)
        elif t < final_start:
            # The middle part of the animation. Slow movement towards top.
            return (h - travel_distance) - (stable_travel_distance * ((t - start_pad_duration - transition_duration) / stable_duration))

        # End animation (title exists through top of the screen)
        return final_travel_distance - \
            final_travel_distance * \
            ((t - final_start) / (transition_duration - end_pad_duration))


    if conf.titleanim == "walrus" and fadeout_title:
        script_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

        txt = text.with_position(('center', 'center'))
        txt = txt.resize(lambda t: walrus_size(clip, t))

        clip = clip.add_mask()
        clip.mask.get_frame = lambda t: bg_circle(clip, t, opacity=0.5)

        comp_clip = CompositeVideoClip([clip, txt] + scoreboards)

        script_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
        audio_name = "ending.mp3"
        sfx = AudioFileClip(os.path.join(script_path, "sfx", audio_name)) \
                    .with_end(dur) \
                    .fx(audio_fadeout, 2) \
                    .fx(audio_fadein, 0.3) \
                    .multiply_volume(INTRO_OUTRO_AUDIO_VOL_FACTOR)

        combined_audio = CompositeAudioClip([comp_clip.audio, sfx])

        comp_clip.audio = combined_audio
    elif conf.titleanim == "walrus" and start_title:
        # Lower the audio volume to match the usually low levels of kyykkä sounds.

        # Wait a bit so that the animation start syncs up with music
        wait = 2.35
        beginning = clip.subclip(0, wait)
        beginning.duration = wait
        clip = clip.subclip(wait, dur)

        logo = get_asset(conf.titleimg)

        scaling_options = {IMG_PYTTY: 0.8, IMG_TAMMER: 0.3,
                           "kyykkamursu.png" : 0.43, "kesamursu" : 0.7 }

        img_scaling = scaling_options[conf.titleimg] if conf.titleimg in scaling_options else 1

        logo = logo.with_position(('center', 'center')) \
                   .resize(lambda t: img_scaling * walrus_size(clip, t))

        txt = text.with_position(lambda t: walrus_movement(clip, t, 0.7, 0.5), relative=True)
        txt = txt.resize(lambda t: walrus_size(clip, t))

        clip = clip.add_mask()
        clip.mask.get_frame = lambda t: bg_circle(clip, t)

        comp_clip = CompositeVideoClip([clip, logo, txt])
        comp_clip.duration = dur - wait

        comp_clip = concatenate_videoclips([beginning, comp_clip])

        script_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
        audio_name = "emotional.mp3"
        sfx = AudioFileClip(os.path.join(script_path, "sfx", audio_name)) \
                .with_end(dur) \
                .fx(audio_fadeout, 4) \
                .multiply_volume(INTRO_OUTRO_AUDIO_VOL_FACTOR)

        combined_audio = CompositeAudioClip([comp_clip.audio, sfx])

        comp_clip.audio = combined_audio
    else:
        (width, height) = text.size
        if conf.titleanim == "upwards":
            txt = text.with_position(
                lambda t: ((w-width)/2, int(upwards_movement(t, clip.h, text))))
        elif (conf.titleanim == "up-down" or conf.titleanim == "walrus"):
            txt = text.with_position(
                lambda t: (w/15, int(up_down_movement(t, clip.h, dur, height))))
        else:
            txt = text.with_position(('center', 'bottom'))
        comp_clip = CompositeVideoClip([clip, txt] + scoreboards)


    comp_clip.duration = dur


    for sb_clip in scoreboards:
        sb_clip.close()

    text.close()
    txt.close()

    return comp_clip




def get_asset(filename):
    script_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    return ImageClip(os.path.join(script_path, "assets", filename))
