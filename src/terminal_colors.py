# Special
BOLD = '\033[1m'
UNDERLINE = '\033[4m'
# Foreground colors
HEADER = '\033[95m'
OKBLUE = '\033[94m'
OKGREEN = '\033[92m'
WARNING = '\033[93m'
CYAN = '\033[36m'
LGRAY = '\033[37m'
FAIL = '\033[91m'
ORANGE = '\033[33m'

# Background colors
DGRAY_BG = '\033[100m'
LRED_BG = '\033[101m'
LGREEN_BG = '\033[102m'
YELLOW_BG = '\033[103m'
LBLUE_BG = '\033[104m'
LPURPLE_BG = '\033[105m'
TEAL_BG = '\033[106m'
WHITE_BG = '\033[107m'

# Ending characterr
ENDC = '\033[0m'

def b(code, string):
    return f"{code}{string}{ENDC}"

def bold(s): return b(BOLD, s)
def header(s): return b(HEADER, s)
def warn(s): return b(WARNING, s)
def ok(s): return b(OKGREEN, s)
def blue(s): return b(OKBLUE, s)
def cyan(s): return b(CYAN, s)
def lgray(s): return b(LGRAY, s)
def fail(s): return b(FAIL, s)
def orange(s): return b(ORANGE, s)

def dgray_bg(s): return b(DGRAY_BG, s)
def lred_bg(s): return b(LRED_BG, s)
def lgreen_bg(s): return b(LGREEN_BG, s)
def yellow_bg(s): return b(YELLOW_BG, s)
def lblue_bg(s): return b(LBLUE_BG, s)
def lpurple_bg(s): return b(LPURPLE_BG, s)
def teal_bg(s): return b(TEAL_BG, s)
def white_bg(s): return b(WHITE_BG, s)



def test_colors():
    print("Testing colors:\n***************")

    print(warn("warn"))
    print(ok("ok"))
    print(blue("blue"))
    print(lgray("lgray"))
    print(header("header"))
    print(fail("fail"))
    print(cyan("cyan"))
    print(orange("orange"))

    print("")

    print(bold(warn("bold warn")))
    print(bold(ok("bold ok")))
    print(bold(blue("bold blue")))
    print(bold(lgray("bold lgray")))
    print(bold(header("bold header")))
    print(bold(fail("bold fail")))
    print(bold(cyan("bold cyan")))
    print(bold(orange("bold orange")))

    print("")

    print(dgray_bg("dgray_bg"))
    print(lred_bg("lred_bg"))
    print(lgreen_bg("lgreen_bg"))
    print(header(yellow_bg("yellow_bg")))
    print(lblue_bg("lblue_bg"))
    print(lpurple_bg("lpurple_bg"))
    print(teal_bg("teal_bg"))
    print(header(white_bg("white_bg")))


    print("***************\n")

if __name__ == "__main__":
    test_colors()
