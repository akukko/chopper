from walrus_animation import *
from constants import *
from colours import *
from gizeh_text_clip import gizeh_text

from moviepy.editor import TextClip, ColorClip

from moviepy.audio.fx.all import *

from ast import literal_eval as make_tuple
import os

black = "black"
old_set_text_color = "silver"


(hauki_color, orange_color,
 yellow_color, green_color,
 dark_blue_color, turqoise_color,
 light_blue_color, purple_color,
 yellowish_color, light_green_color,
 light_turqoise_color) = colour_pallettes[POINTS]


side_padding = RES_FACTOR * 12
indicator_width = RES_FACTOR * 10

def pad_clip(color, width, height, x_pos, y_pos):
    p_clip = ColorClip((width, height),
            color=color) \
            .with_position((x_pos, y_pos))
    return p_clip

def pad_from_font_size(font_size):
    def round_to(number, multiple):
        return multiple * round(number / multiple)

    return round_to(round(font_size / 4), 2)


def draw_points(x, y, player, pts_font_size, active, points,
                set_score, round_len, conf, from_right=False):

    (main_bg_color, secondary_bg_color,
     indicator_color, dark_color,
     total_score_color, active_bg_color) = colour_pallettes[conf.pallette]


    # Round to closest even number
    pad = pad_from_font_size(pts_font_size)

    throw_font_size = round(pts_font_size * 0.4) #15

    name_bg_color = secondary_bg_color

    orig_x = x
    orig_y = y

    clips = []

    fst = True

    for throw_num, p in points:
        pts_font_color = black

        #  incoming      unused
        if p == -10 or p == -20:
            # use big minus numbers to denote incoming and unused throws.
            # Academic games might have negative points (pappi sisään),
            # but large minuses are unlikely.
            pts_font_color = conf.titlecolor

            # None means not thrown yet
            # -1 means throw wasn't used
            if p == -10:
                points_bg_color = secondary_bg_color
                p = " "
            else:
                points_bg_color = main_bg_color
                p = "-"
        elif p == 0:
            points_bg_color = hauki_color
        elif p <= 0:
            points_bg_color = purple_color
        elif p in [1]:
            points_bg_color = orange_color
        elif p in [2]:
            points_bg_color = yellow_color
        elif p in [3]:
            points_bg_color = yellowish_color
        elif p in [4,5]:
            points_bg_color = green_color
        elif p in [6,7]:
            points_bg_color = turqoise_color
        elif p in [8]:
            points_bg_color = dark_blue_color
        else: # 11+
            points_bg_color = light_blue_color

        pts_clip = TextClip(f"{p}", font_size=pts_font_size,
                    font=conf.pointsfont, color=pts_font_color)

        with_small_bg = pts_clip.on_color((pts_clip.w + pad * 3,
                                            pts_clip.h),
                                            points_bg_color)

        with_bg = with_small_bg.on_color((with_small_bg.w,
                                            with_small_bg.h),
                                            main_bg_color)

        throw_num_clip = TextClip(f"{throw_num}", font_size=throw_font_size,
                                  font=conf.throwfont,
                                  color=conf.titlecolor)

        throw_with_bg = throw_num_clip.on_color((with_bg.w,
                                            throw_num_clip.h + pad // 2),
                                            main_bg_color)

        y_padding = with_bg.h + pad

        x_padding = pad // 2

        def add_x_paddings(pts_gfx, throw_gfx, x_pos, bottom=True):
            # Padding between scores (and between
            # bottom padding, hence the: with_bg.h + pad
            clips.append(pad_clip(main_bg_color, x_padding, pts_gfx.h, x_pos,
                                orig_y - y_padding))
            # Padding between throw numbers
            clips.append(pad_clip(main_bg_color, x_padding, throw_gfx.h, x_pos,
                                orig_y - y_padding - throw_gfx.h))
            if bottom:
                # Bottom padding
                clips.append(pad_clip(main_bg_color, pts_gfx.w + pad, pad,
                                    x_pos - pts_gfx.w - x_padding,
                                    orig_y - pad))

            return x_pos + x_padding


        if fst or (throw_num) % round_len == 0:
            # Extra padding at left edge and betweeen rounds
            fst = False
            x = add_x_paddings(with_bg, throw_with_bg, x, False)

        with_pos = with_bg.with_position((x, orig_y - y_padding))

        throw_with_pos = throw_with_bg \
            .with_position((x, orig_y - y_padding - throw_with_bg.h))


        x += with_pos.w
        clips.append(with_pos)
        clips.append(throw_with_pos)

        x = add_x_paddings(with_bg, throw_with_bg, x)

        y = min(y, orig_y - y_padding - throw_with_bg.h)

    name_clip = TextClip(f"{player}", font_size=pts_font_size,
                        font=conf.namefont, color=conf.titlecolor)
    name_with_bg = name_clip.on_color((name_clip.w, name_clip.h + pad),
                                      name_bg_color)

    set_score_font_color = conf.titlecolor

    score_font_pad = 0
    score_font = conf.namefont

    throws_incoming = any([t == -10 for _, t in points])
    if any([t == -20 for _, t in points]) or not throws_incoming:
        # The Redwing font is a bit higher than the player font,
        # so drop it down a little.
        score_font_pad = pad
        score_font = conf.scorefont
        set_score_font_color = conf.lightscorefontcolor

    set_score_clip = TextClip(f"{set_score}", font_size=pts_font_size,
                        font=score_font, color=set_score_font_color)

    set_with_bg = set_score_clip.on_color((set_score_clip.w, set_score_clip.h),
                                          name_bg_color)

    y -= name_with_bg.h

    # Background for nameclip
    clips.append(ColorClip((x - orig_x, name_with_bg.h), color=name_bg_color) \
                   .with_position((orig_x, y)))

    name_box_pad = pad * 2
    clips.append(name_with_bg.with_position((orig_x + name_box_pad, y)))
    clips.append(set_with_bg.with_position((x - set_with_bg.w - name_box_pad,
                                            y + score_font_pad))) # Pad if Redwing

    # If the points need to be shown on the right side of the screen,
    # adjust everything from left side to right
    leftmost_x_position = orig_x
    if from_right:
        right_clips = []
        for c in clips:
            (clip_x, clip_y) = c.pos(0)
            final_x_position = orig_x - (x - orig_x) + (clip_x - orig_x)
            right_clips.append(c.with_position((final_x_position, clip_y)))
            leftmost_x_position = min(leftmost_x_position, final_x_position)
        clips = right_clips

    return clips, y, (x - orig_x, orig_y - y), (leftmost_x_position, y)
