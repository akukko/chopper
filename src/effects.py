#!/usr/bin/env python3

import cv2
from PIL import Image, ImageFilter
import numpy as np

from matplotlib import pyplot as plt

#from moviepy.video.tools.segmenting import find_objects
from moviepy.editor import ImageClip


def s(img):
    #showable_image = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    showable_image = img

    winname = "window"
    cv2.namedWindow(winname)        # Create a named window
    cv2.moveWindow(winname, 40,30)  # Move it to (40,30)
    cv2.imshow(winname, img)
    cv2.waitKey(0)

def cut_out_largest_person(img, gray_scale=True, darken_bg=False):


    orig_img = img.copy()

    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    lower_green = np.array([20,25,0])
    upper_green = np.array([100,255,255])
    mask = cv2.inRange(hsv, lower_green, upper_green)

    lower = np.array([0,0,100])
    upper = np.array([360,10,360])
    mask3 = cv2.inRange(hsv, lower, upper)

    lower = np.array([100,10,100])
    upper = np.array([150,30,200])
    mask4 = cv2.inRange(hsv, lower, upper)

    result = orig_img.copy()
    result = cv2.cvtColor(result, cv2.COLOR_BGR2BGRA)

    total_mask = mask  |  mask3 | mask4
    result[:, :, 3] = (255 - total_mask)

    alternative_mask = np.where((total_mask==255),0,1).astype('uint8')
    alternative_result =  orig_img.copy() * alternative_mask[:, :, np.newaxis]

    gray= cv2.cvtColor(alternative_result, cv2.COLOR_BGRA2GRAY)

    edges= cv2.Canny(gray, 0, 1)

    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(4,4))
    dilated = cv2.dilate(edges, kernel)

    contours, hierarchy= cv2.findContours(dilated.copy(),
                                        cv2.RETR_EXTERNAL,
                                        cv2.CHAIN_APPROX_NONE)

    largest_item = sorted(contours, key=cv2.contourArea, reverse= True)[0]

    '''
    # Show largest object
    contoured_img = orig_img.copy()
    for c in contours:
        cv2.drawContours(contoured_img, largest_item, -1, (0,255,0),2)

    s(contoured_img)
    '''


    mask = np.zeros(total_mask.shape, np.uint8)
    cv2.fillPoly(mask, pts =[largest_item], color=(255,255,255))

    if not gray_scale:
        # This makes the background transparent
        result = orig_img.copy()
        result = cv2.cvtColor(result, cv2.COLOR_BGR2BGRA)

        mask = mask[:]
        min_mask = np.zeros(total_mask.shape, np.uint8)
        result[:, :, 3] = (np.maximum(min_mask, mask))
    else:
        fg = orig_img.copy()
        fg = cv2.bitwise_and(fg, fg, mask=(mask))

        gray = cv2.cvtColor(orig_img.copy(), cv2.COLOR_BGR2GRAY)
        gray = cv2.cvtColor(gray, cv2.COLOR_GRAY2BGR)

        bg = cv2.bitwise_and(gray, gray, mask=(255 - mask))
        if darken_bg:
            black_mask = np.zeros(bg.shape, np.uint8)
            alpha = 0.5
            bg = cv2.addWeighted(black_mask, alpha, bg, 1 - alpha, 0.0)

    result = fg + bg
    cv2.imwrite("test.png", cv2.cvtColor(result, cv2.COLOR_BGRA2RGBA))

    cv2.destroyAllWindows()

    return result
