from terminal_colors import *

from titles import build_scoreboard



class PlrStats:
    def __init__(self, plr, t1, t2, t3, t4):
        self.plr = plr
        self.t1 = t1
        self.t2 = t2
        self.t3 = t3
        self.t4 = t4


    def __str__(self):
        return self.plr + ": " + self.t1 + " " + self.t2 + " " + self.t3 + " " + self.t4

class Scores:
    def __init__(self, ht, at, ht_short, at_short, hs1, as1, hs2, as2, stats):
        self.ht = ht
        self.at = at
        self.ht_short = ht_short
        self.at_short = at_short
        self.hs1 = hs1 # score from first set
        self.as1 = as1
        self.hs2 = hs2 # score from second set
        self.as2 = as2

        self.stats = stats


class MutableData:
    def __init__(self, s_gfx, plrs, points, info, titles, f_title):
        self.scoreboard_graphics = s_gfx
        self.players = plrs
        self.info = info
        self.points = points
        self.titles = titles
        self.first_title = f_title

def stats_in_order(stats):
    ordered_stats = []

    fst_round = []
    snd_round = []
    for s in stats[:8]:
        fst_round.append(s.t1)
        fst_round.append(s.t2)
        snd_round.append(s.t3)
        snd_round.append(s.t4)
    ordered_stats.extend(fst_round)
    ordered_stats.extend(snd_round)
    fst_round = []
    snd_round = []
    for s in stats[8:]:
        fst_round.append(s.t1)
        fst_round.append(s.t2)
        snd_round.append(s.t3)
        snd_round.append(s.t4)
    ordered_stats.extend(fst_round)
    ordered_stats.extend(snd_round)

    return ordered_stats

def get_ordered_stats(scores):
    print(header(bold("\nValidating stats:")))

    if scores:
        stats = scores.stats
        # validate stats

        if len(stats) != 16:
            print(fail(f"Invalid amount of players in stats: {len(stats)}. Aborting."))
            return None

        
        t1_f = [0, 1, 4, 5]
        t1_s = [10, 11, 14, 15]
        t2_f = [2, 3, 6, 7]
        t2_s = [8, 9, 12, 13]

        hs1 = -80
        hs2 = -80

        as1 = -80
        as2 = -80

        # Check that stats match with set results
        for i, s in enumerate(stats):
            for t in [s.t1, s.t2, s.t3, s.t4]:
                if t != "H" and t != "E":
                    try:
                        x = int(t)
                        if i in t1_f:
                            hs1 += x
                        elif i in t1_s:
                            hs2 += x
                        elif i in t2_f:
                            as1 += x
                        elif i in t2_s:
                            as2 += x
                    except:
                        print(fail(f"Invalid stats: {s}. Aborting."))
                        return
                elif t == "E":
                    if i in t1_f:
                        hs1 += 1
                    elif i in t1_s:
                        hs2 += 1
                    elif i in t2_f:
                        as1 += 1
                    elif i in t2_s:
                        as2 += 1
        
        mismatch = []

        if int(scores.hs1) != hs1:
            mismatch.append(f"{scores.ht_short} first set: {hs1} (actual {scores.hs1})")
        if int(scores.hs2) != hs2:
            mismatch.append(f"{scores.ht_short} second set: {hs2} (actual {scores.hs2})")
        if int(scores.as1) != as1:
            mismatch.append(f"{scores.at_short} first set: {as1} (actual {scores.as1})")
        if int(scores.as2) != as2:
            mismatch.append(f"{scores.at_short} second set: {as2} (actual {scores.as2})")
            
        if mismatch:
            for m in mismatch:
                print(fail(f"Stats mismatch: {m}"))
            return None

        players = []
        for s in stats:
            players.append(s.plr)
        plrs = players[:8]
        plrs.extend(plrs)
        second_set_plrs = players[8:]
        plrs.extend(second_set_plrs)
        plrs.extend(second_set_plrs)
        players = plrs
        ordered_stats = stats_in_order(stats)
    else:
        return None
    return ordered_stats, players

def get_scoreboard_stats(scores, conf):
    maybe_stats = get_ordered_stats(scores)
    scoreboards_and_players = None
    if maybe_stats:
        (ordered_stats, players) = maybe_stats
        scoreboard_graphics = []
        throw_amount = 1
        fst_set_score1 = conf.starting_score
        fst_set_score2 = conf.starting_score
        snd_set_score1 = None
        snd_set_score2 = None

        while ordered_stats:
            gfx_params, ordered_stats, throw_amount, \
            fst_set_score1, fst_set_score2, \
            snd_set_score1, snd_set_score2 \
                = build_scoreboard(ordered_stats, throw_amount,
                                    scores, fst_set_score1, fst_set_score2,
                                    snd_set_score1, snd_set_score2, conf)

            scoreboard_graphics.append(gfx_params)
        scoreboards_and_players = (scoreboard_graphics, players)
    return scoreboards_and_players
