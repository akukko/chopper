from terminal_colors import *
from constants import *
from stats import *


from moviepy.editor import AudioFileClip, CompositeAudioClip, \
    concatenate_videoclips


import glob
import os
import re
from ast import literal_eval as make_tuple


def get_lines(file):
    try:
        with open(file, "r", encoding='utf-8') as f:
            lines = f.readlines()
        without_comments = []
        for line in lines:
            if not line.strip().startswith("#") and line.strip():
                without_comments.append(line.strip())
        return without_comments
    except:
        return []

def titles_from_scores(scores, conf):
    if not scores:
        return None


    start_title = f"{conf.comp_name}:\n{scores.ht} - {scores.at}"
    def score(team, score):
        return f"{team:<{MAX_TEAM_NAME_LENGTH}} {score:>6}"
    def final_scores(team, s1, s2):
        return final(team, int(s1), int(s2), int(s1) + int(s2))
    def final(a, b, c, d):
        return f"{a:<{MAX_TEAM_NAME_LENGTH}} {b:>6}  {c:>6} {d:>7}"

    delim = "─"
    first_set_titles = score("Joukkue", "1. erä")
    first_set_scores = first_set_titles + "\n" + \
        len(first_set_titles) * delim + "\n" + \
        score(scores.ht, scores.hs1) + "\n" + score(scores.at, scores.as1)

    final_titles = final("Joukkue", "1. erä", "2. erä", "Ottelu")
    final_scores = final_titles + "\n" + \
        len(final_titles) * delim + "\n"  + \
        final_scores(scores.ht, scores.hs1, scores.hs2) + "\n"  + \
        final_scores(scores.at, scores.as1, scores.as2)

    return [start_title, first_set_scores, final_scores]


ready_scores = "scoreboards"
ready_players = "players"
ready_points = "points"

def get_project_files(input_dir, titlefile, statsfile, recursive, conf):
    videofiles = []
    datafiles = []
    titles = []

    ignored_files = get_ignored_files(input_dir, recursive)

    if ignored_files:
        print(bold("Ignoring files matching with .chopignore file:"))
        for ign in ignored_files:
            print(ign)

        print("")

    ready_scoreboard_graphics = []
    ready_player_names = []
    ready_points_graphics = []
    ready_titles = []
    ready_info = None

    stats = []
    set_results = []
    fst_team_long = None
    snd_team_long = None
    fst_team_short = None
    snd_team_short = None

    fst_team_temp_short = None

    for root, _, files in os.walk(input_dir):
        file_set = set(files)
        new_titles = []
        found_valid_videos = False
        for fname in sorted(files):
            if fname.lower().endswith("mp4") \
                or fname.lower().endswith("mov") \
                or fname.lower().endswith("mts"):
                base_name = ".".join(fname.split('.')[:-1])
                cut_file = f"{base_name}.txt"
                if cut_file in file_set and os.path.join(root, fname) not in ignored_files:
                    if new_titles:
                        titles.extend(new_titles)
                    videofiles.append(os.path.join(root, fname))
                    datafiles.append(os.path.join(root, cut_file))
                    found_valid_videos = True

            elif fname.lower() == titlefile:
                t = get_lines(os.path.join(root, titlefile))
                if found_valid_videos:
                    titles.extend(t)
                else:
                    new_titles.extend(t)
            elif not ready_scoreboard_graphics and fname.lower() == ready_scores:
                # Use scoreboard graphics that have been generated by outside program
                for l in get_lines(os.path.join(root, ready_scores)):
                    ready_scoreboard_graphics.append(make_tuple(l))

            elif not ready_player_names and fname.lower() == ready_players:
                # Use players graphics that have been generated by outside program
                ls = get_lines(os.path.join(root, ready_players))
                for l in ls:
                    if l == "None":
                        ready_player_names.append(None)
                    else:
                        ready_player_names.append(l)
            elif not ready_info and fname.lower() == "info":
                # Use players graphics that have been generated by outside program
                ls = get_lines(os.path.join(root, "info"))
                for l in ls:
                    ready_info = make_tuple(l)
                    break

            elif not ready_points_graphics and fname.lower() == ready_points:
                for l in get_lines(os.path.join(root, ready_points)):
                    ready_points_graphics.append(make_tuple(l))
            # There can only be one statsfile. It should probably
            # always be at the top level directory.
            elif not stats and fname.lower() == statsfile:
                stats = []
                ls = get_lines(os.path.join(root, statsfile))
                for l in ls:
                    p = re.compile(r"^(?P<plr>\S+\s\S+),\s+\S+\s*\S*\s+(?P<fst>-?\S+)\s+(?P<snd>-?\S+)\s+(?P<thd>-?\S+)\s+(?P<fth>-?\S+)\s+\S+\s+\S+\s*$")
                    m = p.search(l)
                    if m:
                        stats.append(PlrStats(m.group("plr"), m.group("fst"), m.group("snd"), m.group("thd"), m.group("fth")))
                    else:
                        p = re.compile(r"\d.*Erän tulos\s+(?P<fst_team_short>\S+\s*\S*)\s+:\s+(?P<fst_team_res>-?\d+)\s*–\s*(?P<snd_team_res>-?\d+)\s+:\s+(?P<snd_team_short>\S+\s*\S*)")
                        m = p.search(l)
                        if m:
                            set_results.append(m.group("fst_team_res"))
                            set_results.append(m.group("snd_team_res"))
                            fst_team_short = m.group("fst_team_short")
                            snd_team_short = m.group("snd_team_short")

                            if fst_team_temp_short != fst_team_short:
                                x = fst_team_long
                                fst_team_long = snd_team_long
                                snd_team_long = x
                                fst_team_temp_short = fst_team_short

                        else:
                            p = re.compile(r"^(?P<long1>.+)\s+\((?P<short1>.+)\)\s+–\s+(?P<long2>.+)\s+\((?P<short2>.+)\),\s+\S+$")
                            m = p.search(l)
                            if m:
                                fst_team_long       = m.group("long1")
                                fst_team_temp_short = m.group("short1")
                                snd_team_long       = m.group("long2")

                if len(stats) != 16:
                    print(warn("Wrong amount of players in players file.\n"))

        if not recursive:
            break

    scores = None
    if fst_team_long and snd_team_long and fst_team_short and \
        fst_team_short and stats and set_results:
        scores = Scores(fst_team_long, snd_team_long,
                        fst_team_short, snd_team_short,
                        set_results[0], set_results[1],
                        set_results[2], set_results[3], stats)

    # if scores exist, make titles and scoreboards from them.
    # Ready-made graphics always override others
    ready_scores_and_players = None
    if ready_scoreboard_graphics and ready_player_names \
       and ready_points_graphics and ready_info:
        ready_scores_and_players = \
            (ready_scoreboard_graphics, ready_player_names,
             ready_points_graphics, ready_info)
    elif ready_points_graphics and ready_info:
        ready_scores_and_players = \
            (None, None, ready_points_graphics, ready_info)

    scoreboards_and_players = \
        ready_scores_and_players or get_scoreboard_stats(scores, conf)
    ttl = ready_titles or titles_from_scores(scores, conf) or titles

    return videofiles, datafiles, ttl, scoreboards_and_players


def get_ignored_files(input_dir, recursive):
    ignored_files = set()

    globs = get_lines(os.path.join(input_dir, ".chopignore"))
    for g in globs:
        stripped = g.strip()

        def add_ignore(match):
            ignored = os.path.join(input_dir, match)
            if os.path.isdir(ignored):
                if recursive:
                    for f in os.listdir(ignored):
                        add_ignore(os.path.join(ignored, f))
            elif os.path.isfile(ignored):
                ignored_files.add(ignored)

        for match in glob.glob(stripped, recursive=True):
            add_ignore(match)
    return ignored_files

def add_audio(clip, audio_path, start_t=None, pad=None):
    script_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    sfx = AudioFileClip(script_path + audio_path)

    if start_t != None:
        sfx = sfx.subclip(start_t)

    if pad == None:
        if clip.audio:
            combined_audio = CompositeAudioClip([clip.audio, sfx])
        else:
            combined_audio = sfx

        clip.audio = combined_audio
        return clip
    else:
        fst = clip.subclip(0, pad)
        snd = clip.subclip(pad)
        if snd.audio:
            combined_audio = CompositeAudioClip([snd.audio, sfx])
        else:
            combined_audio = sfx
        snd.audio = combined_audio

        return concatenate_videoclips([fst, snd])


