from terminal_colors import *
from constants import *
from stats import *
from effects import cut_out_largest_person
from colours import *
from titles import *
from points import draw_points, pad_from_font_size
from animation import up_and_stay, up_up_movement
from files import add_audio
from gizeh_text_clip import gizeh_text


from moviepy.editor import TextClip, CompositeVideoClip, \
    concatenate_videoclips, ColorClip


# These match the Haskell
home_throwing = "Home"
away_throwing = "Away"
last_home_throw = "LastHome"
last_away_throw = "LastAway"
set_end = "SetEnd"
game_end = "GameEnd"


pts_pad = RES_FACTOR * 10

title_amt = 0
cut_count = 0

def parse_cuts(vid, filename, data, dry, preview, frames, conf):
    global title_amt
    global cut_count

    cuts = []
    if isinstance(data, list):
        titles = data
        players = None
        scoreboard_graphics = None
        points = None
        info = None
        first_title = data[0] if len(data) > 0 else ""
    else:
        scoreboard_graphics  = data.scoreboard_graphics
        players        = data.players
        titles         = data.titles
        points         = data.points
        info           = data.info
        first_title    = data.first_title

    with open(filename, "r") as f:
        lines = [l for l in (line.strip() for line in f) if l]

        # validation
        start = None
        title_start = None
        checked_starting_title_length = False


        print("\n" + header(bold("Validating cutfile:")))

        def vld_warn(s):
            print(warn(s))

        def vld_fail(s):
            print(fail(s))

        for font in [conf.starttitlefont, conf.endtitlefont,
                     conf.scorefont, conf.scoreboardfont, conf.pointsfont]:
            if font not in TextClip.list("font"):
                for f in TextClip.list("font"):
                    print(f)

                vld_fail(f"Chosen font {font} wasn't found, please select a new one.")

                return None




        time = None
        title_amt = 0

        for line in lines:
            if line.startswith("*") or line.startswith("|"):
                title_amt += 1

            if line.startswith("|"):
                end_title_time = float(line[1:].strip())
                minimum = 14
                maximum = 25
                title_length =  end_title_time - time
                if title_length > maximum:
                    vld_warn(f"The end title length is too long ({title_length} s). Aim for {minimum}-{maximum} s.")
                elif title_length < minimum:
                    vld_warn(f"The end title length is too short ({title_length} s). Aim for {minimum}-{maximum} s.")


            # TODO: make the < case take into account the modifiers also
            if line.startswith(">") or line.startswith("< ") or line.startswith("*") or line.startswith("|"):
                time = float(line[1:].strip())
                if not checked_starting_title_length and title_start != None:
                    title_length = time - title_start
                    minimum = 14
                    maximum = 25
                    checked_starting_title_length = True
                    if title_length < minimum:
                        vld_fail(f"The starting title length is too short ({title_length} s). Aim for {minimum}-{maximum} s.")
                        return None
                    elif title_length > maximum:
                        vld_warn(f"The starting title length is too long ({title_length} s). Aim for {minimum}-{maximum} s.")
                        title_start = -1

            # Check if the first ever title in the video is longer than x seconds.
            if (line.startswith("*") and not checked_starting_title_length):
                title_start = float(line[1:].strip())
            else:
                checked_starting_title_length = True

            if line.startswith(">"):
                start = float(line[1:].strip())
            elif line.startswith("<"):
                if start == None:
                    vld_warn(f"Cut file had '<' symbol even though there was no preceding '>'.")
                    continue
                end = float(line.split()[1].strip())
                time_low_limit = 4

                if (end - start) < time_low_limit:
                    vld_warn(f"Cut {start:.2f} -> {end:.2f} was less than {time_low_limit} s.")

                if (end - start) < TYPICAL_THROW_LENGTH + 2:
                    vld_warn(f"Cut {start:.2f} -> {end:.2f} was less than set end animation limit s.")

                dur = end - start
                start = None
                cut_count += 1

        if data:
            if title_amt > 3:
                vld_fail(f"Was using stats and expecting at most three titles, but title amount is {title_amt}.")
                return None
        elif title_amt > len(titles):
            vld_fail(f"Amount of titles in cutfile ({title_amt}) doesn't match titlefile ({len(titles)}).")
            return None


        if not cut_count in [32, 64]:
            vld_warn(f"Cut count was {cut_count}.")

        if dry:
            print(bold(cyan("\nStopping execution because of the --dry-run (-d) argument")))
            return None

        start = None
        end = None
        title = None
        clip = None
        title_start = None



        for line in lines:
            # This enables previewing a smaller amount of
            # frames without modifying the cutfile.
            if frames and frames >= 0 and len(cuts) >= frames:
                break


            # TODO: Rewrite this if clause so that it is easier to add a new character possibilities
            if line.startswith(">"):
                start = float(line[1:].strip())
                if title_start:
                    # MAKE START TITLE
                    if info and points:
                        home = points[0][1]
                        away = points[0][2]
                        start_title_clip = vid.subclip(title_start, start)
                        clip = draw_start_title(start_title_clip, info,
                                                home, away,conf)
                        clip = add_audio(clip, SFX_CHILL)\
                            .with_duration(clip.duration).audio_fadeout(2)
                    else:
                        clip = build_title(vid, title_start, start,
                                        titles.pop(0), conf.starttitlefont,
                                        conf.titlesize, conf, first_title)
                    cuts.append(clip)
                    title_start = None
                    first_title = False
                end = None
                title = None
            elif line.startswith("<"):
                skip_scoreboards = False


                end = float(line.split()[1].strip())
                if line.startswith("<vol"):
                    clip = vid.subclip(start, end).multiply_volume(conf.volfactor)
                elif line.startswith("<slow"):
                    clip = vid.subclip(start, end).speedx(conf.speedfactor)
                elif line.startswith("<sfx_move"):
                    clip = add_audio(vid.subclip(start, end),
                                     SFX_LAIDALLA)
                elif line.startswith("<sfx_fail"):
                    clip = add_audio(vid.subclip(start, end),
                                     SFX_SIPULI)
                elif line.startswith("<sfx_hiivatti"):
                    clip = add_audio(vid.subclip(start, end) \
                                     .multiply_volume(conf.volfactor),
                                     SFX_HIIVATTI, pad=TYPICAL_THROW_LENGTH)
                elif line.startswith("<sfx_paska"):
                    clip = add_audio(vid.subclip(start, end) \
                                     .multiply_volume(conf.volfactor),
                                     SFX_PASKA, pad=TYPICAL_THROW_LENGTH)
                elif line.startswith("<vfx_freeze"):

                    blue = (73, 97, 158)
                    red = (255, 50, 50)
                    color = red
                    resize_factor = 8
                    plr_name = "VP"

                    clip = vid.copy().subclip(start, end)

                    tfreeze = 0
                    im_freeze = clip.to_ImageClip(tfreeze)
                    img = im_freeze.img
                    im = cut_out_largest_person(img)
                    intro_cutout = ImageClip(im)


                    intro_cutout = intro_cutout.resize(resize_factor)
                    intro_cutout = intro_cutout\
                        .with_position(((clip.w - intro_cutout.w) / 2 + 250,
                                        (clip.h - intro_cutout.h) / 2 - 500 ))
                                               #.with_position((-2000, -2000))
                    txt = gizeh_text(plr_name, font='Rockwell',
                                          font_size=400, stroke_color=(0,0,0),
                                          stroke_width=20,
                                          fill_color=(255,255,255))

                    freeze_duration = 5

                    bg = ColorClip(size=clip.size, color=color)
                    intro_clip = CompositeVideoClip(
                                        [bg,
                                         intro_cutout,
                                         txt.with_position((1000,700))]) \
                                         .with_duration(freeze_duration)

                    intro_clip = add_audio(intro_clip, SFX_ROCKSTAR, 0.75) \
                        .with_duration(freeze_duration).multiply_volume(0.7)

                    clip =  concatenate_videoclips([intro_clip, clip])

                    skip_scoreboards = True
                else:
                    clip = vid.subclip(start, end)

                if points and info and not skip_scoreboards:
                    clip = make_point_graphics(clip, points, info,
                                               clip.duration, preview, conf)
                cuts.append(clip)

                start = None
                title = None
            elif line.startswith("*") or line.startswith("|"):
                if title:
                    # title is after another title
                    title_start = title
                elif start and not end:
                    # title is after a clip start (>)
                    title_start = start
                elif end:
                    # title is after a clip end (<)
                    title_start = end
                else:
                    # title is at the very beginning
                    title_start = float(line[1:].strip())
                    continue

                title = float(line[1:].strip())

                clip = vid.subclip(title_start, title)
                w, h = clip.size

                clips = [clip]
                if points and info:
                    # Set end points overview

                    tmp_dur = clip.duration
                    (throwing, _) = points[0][0]

                    music_start = 0
                    if throwing == set_end:
                        end_gfx = make_set_end_graphics(points, info, w,
                                                        h, tmp_dur, conf)

                    elif throwing == game_end:
                        music_start = 14.8
                        end_gfx = make_game_end_graphics(points, info, w,
                                                        h, tmp_dur, conf)

                    clips.extend(end_gfx)
                    clip = CompositeVideoClip(clips)
                    clip.duration = tmp_dur
                    clip = add_audio(clip, SFX_CHILL, start_t=music_start) \
                            .with_duration(clip.duration)\
                            .audio_fadeout(2)

                    clip.duration = tmp_dur
                else:
                    # Just some text title
                    clip = build_title(vid, title_start, title,  titles.pop(),
                                    conf.endtitlefont, conf.scoresize, conf,
                                    False, line.startswith("|"))

                cuts.append(clip)


                title_start = None
            elif line.startswith("#"):
                # Ignore comment lines
                pass
        if start:
            clip = vid.subclip(start, vid.duration)
            cuts.append(clip)

    if not isinstance(data, list):
        data.scoreboard_graphics = scoreboard_graphics
        data.players             = players
        data.titles              = titles
        data.points              = points
        data.first_title         = first_title

    if frames and frames < 0:
        return cuts[frames:], data

    return cuts, data




def make_point_graphics(clip, points, info, throwing_clip_dur, preview, conf):

    (main_bg_color, secondary_bg_color,
    indicator_color, dark_color,
    total_score_color, active_bg_color) = colour_pallettes[conf.pallette]

    # Draw scoreboard to upper corner
    #sb = draw_scoreboards(clip, *gfx, conf)
    sb = []
    w, h = clip.size

    # Draw points listing to the bottom of the screen


    ((throwing, setResult), home, away, homeSet,
        awaySet, pointsHome, pointsAway) = points.pop(0)

    home_active = throwing in [home_throwing, last_home_throw]
    away_active = throwing in [away_throwing, last_away_throw]
    round_len = info[0]


    pts_font_size = RES_FACTOR * 30
    point_overlays1, pts_max_height, home_size, home_pos = \
        draw_points(pts_pad, h - pts_pad * 2, home, pts_font_size,
                    home_active, pointsHome, homeSet, round_len, conf)


    point_overlays2, _, away_size, away_pos = \
        draw_points(w - pts_pad, h - pts_pad * 2, away, pts_font_size,
                    not home_active, pointsAway, awaySet, round_len, conf, True)

    # Add a border around points graphics for the active team/player
    border_width = pad_from_font_size(pts_font_size) // 2
    active_bg = []
    if home_active or away_active:
        if home_active:
            (active_x, active_y), active_pos = home_size, home_pos
        else:
            (active_x, active_y), active_pos = away_size, away_pos

        active_bg = [ColorClip((active_x + border_width, active_y + border_width),
                               color=indicator_color) \
            .with_position((active_pos[0] - border_width // 2,
                            active_pos[1] - border_width // 2))]

    # Make animation when set ends
    def animation(size, pos, total, animation_duration):
        if (animation_duration < TYPICAL_THROW_LENGTH < 0):
            raise Exception("Last throw of the set was too short for animation")

        set_end_text = TextClip(f"ERÄTULOS   {total}",
                                font_size=RES_FACTOR * 50,
                                color=conf.lightscorefontcolor,
                                font=conf.scorefont)

        set_end = set_end_text.on_color(size, main_bg_color)
        f = lambda t: (pos[0], int(up_and_stay(t, h, animation_duration, set_end.h,
                                               pos[1], TYPICAL_THROW_LENGTH)))
        return set_end.with_position(f)

    set_end_animation = []

    if throwing == last_home_throw:
        set_end_animation.append(animation(home_size, home_pos,
                                            setResult[0], throwing_clip_dur))

    elif throwing == last_away_throw:
        set_end_animation.append(animation(away_size, away_pos,
                                            setResult[0], throwing_clip_dur))
    # Make an opaque background for the points
    black = (0,0,0)
    opaque_pad = pts_max_height - pts_pad
    pts_opaque_bg = ColorClip((w, h - opaque_pad), black) \
                    .with_position((0, opaque_pad)) \
                    .with_opacity(0.4)

    # Make a red line border the opaque bg
    line_width = 3
    red_line = ColorClip((w, line_width), indicator_color) \
                    .with_position((0, opaque_pad - line_width))

    # Draw tammer logo to middle of banner

    logo = get_asset(IMG_TAMMER_SMALL)
    logo = logo.with_position((w/2 - logo.w/2,
                                pts_max_height + pts_pad))

    # opaque elements don't work in previews, and end up blocking
    # other elements in the preview, so
    # don't include them if we're just previewing
    opaque_elements = \
        [] if preview else [pts_opaque_bg, red_line, logo]

    overlays = sb \
        + opaque_elements \
        + active_bg \
        + list(point_overlays1) \
        + list(point_overlays2) \
        + set_end_animation

    clip = CompositeVideoClip([clip] + overlays)
    for sb_clip in overlays:
        sb_clip.close()


    clip.duration = throwing_clip_dur
    return clip

def make_set_end_graphics(points, info, w, h, dur, conf):

    (_, home, away, homeSet, awaySet, homePoints, awayPoints) = points.pop(0)

    round_len = info[0]

    pts_font_size = RES_FACTOR * 50

    x_from_edge = 100
    y_from_edge = h - pts_pad * 2

    point_overlays1, _, home_size, _ = \
        draw_points(x_from_edge, y_from_edge, home, pts_font_size,
                    False, homePoints, homeSet, round_len, conf)


    point_overlays2, _, away_size, _ = \
        draw_points(w - x_from_edge, y_from_edge, away, pts_font_size,
                    False, awayPoints, awaySet, round_len, conf, True)


    def adjust(clip, y_diff):
        (x, y) = clip.pos(0)
        f = lambda t: (x, int(up_up_movement(t, h, dur, clip.h, y + y_diff)))
        return clip.with_position(f)

    adjusted_overlays = []
    for ol in point_overlays1:
        adjusted = adjust(ol, - home_size[1] - h/2)
        adjusted_overlays.append(adjusted)

    for ol in point_overlays2:
        adjusted = adjust(ol, away_size[1] - h/2)
        adjusted_overlays.append(adjusted)

    return adjusted_overlays

def make_game_end_graphics(points, info, w, h, dur, conf):

    ((_, [home_fst_set, away_fst_set]), home, away, homeSet, awaySet,
     homePoints, awayPoints) = points.pop(0)

    round_len = info[0]

    pts_font_size = RES_FACTOR * 50

    x_from_edge = 100
    y_from_edge = h - pts_pad * 2

    half = len(homePoints) // 2
    if half != (len(awayPoints) // 2):
        raise Exception("when game ends, both teams/players should" +
                        " have same amount of points to display")

    def fst_set(t, score): return f"{t}: {score}"




    # home points
    fstSetHomePoints = homePoints[:half]
    sndSetHomePoints = homePoints[half:]

    point_overlays1, _, home_size, _ = \
        draw_points(x_from_edge, y_from_edge, fst_set(home, home_fst_set + homeSet),
                    pts_font_size, False, fstSetHomePoints, home_fst_set,
                    round_len, conf)

    point_overlays2, _, home_size, _ = \
        draw_points(x_from_edge, y_from_edge, " ", pts_font_size,
                    False, sndSetHomePoints, homeSet, round_len, conf)

    # away points
    fstSetAwayPoints = awayPoints[:half]
    sndSetAwayPoints = awayPoints[half:]

    point_overlays3, _, away_size, _ = \
        draw_points(w - x_from_edge, y_from_edge,
                    fst_set(away, away_fst_set + awaySet),
                    pts_font_size, False, fstSetAwayPoints, away_fst_set,
                    round_len, conf, True)

    point_overlays4, _, away_size, _ = \
        draw_points(w - x_from_edge, y_from_edge, " ",
                    pts_font_size, False, sndSetAwayPoints, awaySet,
                    round_len, conf, True)

    def adjust(clip, y_diff):
        (x, y) = clip.pos(0)
        f = lambda t: (x, int(up_up_movement(t, h, dur, clip.h, y + y_diff)))
        return clip.with_position(f)

    adjusted_overlays = []
    for ol in point_overlays1:
        adjusted = adjust(ol, -1.25 * home_size[1] - h/2)
        adjusted_overlays.append(adjusted)

    for ol in point_overlays2:
        adjusted = adjust(ol, -0.25 * home_size[1] - h/2)
        adjusted_overlays.append(adjusted)

    for ol in point_overlays3:
        adjusted = adjust(ol, 1.25 * away_size[1] - h/2)
        adjusted_overlays.append(adjusted)

    for ol in point_overlays4:
        adjusted = adjust(ol, 2.25 * away_size[1] - h/2)
        adjusted_overlays.append(adjusted)

    return adjusted_overlays
