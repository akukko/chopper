# chopper

Automatic video editor for quickly editing kyykkä videos.

Example of a video produced with chopper: https://youtu.be/Af_v5m7o6B0

(The example video requires some more advanced configuration than what is described in this README, but is nonetheless entirely possible with the latest version of the software.)

## Basic usage

``` python process.py --input <path-to-folder> --output <output-file> --titles <title-file>```

The input folder should contain one or more mp4 videos, and cut files corresponding to the video filename. The cut file contains the timestamps of the desired cuts that chopper should make.

If ran inside the input directory, the process.py doesn't require any arguments. --output will default to "out.mp4", --titles will default to "titles" and the directory in which the process.py is ran is used as input directory.

An example folder could contain files:
* vid1.mp4
* vid2.mp4
* vid1.txt
* vid2.txt

Cut file (the text files) format is the following:

```
> clip start (in seconds)
< clip end (in seconds)
```

## Titles

Cut file format with titles is the following:
```
* title1 start
> clip start / title1 end / title2 start
* title2 end
< clip end / title3 start
* title3 end / title4 start
* title4 end
```

For example, to make one title from 1.5 s to 2 s, a second one from 3 s to 5 s and a third and final one from 5 s to 7.5 s:
```
* 1.5
> 2
< 3
* 5
* 7.5
```

The title functionality assumes that a file matching with the --titles argument (or if no --titles argument is given, default of "titles") is within the input directory. The titles file needs to have the same amount of lines as there are titles in the cut file. The text within each line of the titles file is then displayed at the bottom center of the video for the duration determined in the cut file.

## Automatic timestamps from mpv

The `clients/mpv/` directory contains a README file containing instructions for setting up the mpv script that allows for automatic timestamp generation from mpv. This is the preferred way of gathering the timestamps.

## Known issues
* When merging multiple videos, the order of the videos is only determined by the output of `os.listdir`
* Using the title functionality required ImageMagick, and it needs to be set up correctly. More information here: https://github.com/Zulko/moviepy/issues/693

