{ pkgs ? import <nixpkgs> { } }:

pkgs.mkShell {
  packages = [
    (pkgs.python311.withPackages (ps: with ps; [
      pandas
      cairocffi
      # moviepy
      imageio
      setuptools
      opencv3
      matplotlib
      scipy
      python-lsp-server
      proglog
      decorator
      pkgconfig
      pycairo
      gst-python
      decorator
      proglog
      imageio
      requests
      opencv3
      py-cpuinfo
      pyflakes
      xlsxwriter
      smmap
      whatthepatch
      trustme
      diskcache
    ]))

    pkgs.gcc
    pkgs.cairo.dev
    pkgs.xorg.libxcb.dev
    pkgs.xorg.xorgproto
    pkgs.xorg.libX11.dev
    pkgs.glib.dev
    pkgs.libffi.dev
    pkgs.cmake
    pkgs.zlib
    pkgs.libglvnd
    pkgs.pkg-config
    pkgs.glibc
    # pkgs.xcrypt
    # pkgs.libxcrypt
    pkgs.libgcrypt
    pkgs.opencv
    pkgs.imagemagick
  ];
}

