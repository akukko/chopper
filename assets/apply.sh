for file in ./*
do
    extension="${file##*.}"
    filename="${file%.*}" 
    lutname='_lut'
    fullname="$filename$lutname.$extension"

    ffmpeg -i "$file" -vf lut3d=~/projects/chopper/assets/lut.cube -s 1920x1080 -c:v dnxhd -pix_fmt yuv422p -b:v 120M "$fullname" &
done

